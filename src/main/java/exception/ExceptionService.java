package exception;

public class ExceptionService extends Exception {

    private String errorMessage;

    public ExceptionService(String message) {
        super(message);
        this.errorMessage = message;
    }

}