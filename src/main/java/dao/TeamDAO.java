package dao;

import beans.Team;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class TeamDAO extends DAO<Team> {

    public TeamDAO(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public boolean add(Team element) {
        try{
            em.persist(element);
            return true;
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }


    public boolean modify(Team element) {
        return false;
    }

    public Team getTeamByName(String name){
        try {
            Query query = em.createQuery("SELECT team FROM Team team WHERE team.teamName =: tname",Team.class);
            query.setParameter("tname",name);
            return (Team) query.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Team> getAll() {
        try{
            return em.createQuery("SELECT team FROM Team team", Team.class).getResultList();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
