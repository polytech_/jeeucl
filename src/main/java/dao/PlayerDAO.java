package dao;

import beans.Player;

import javax.persistence.EntityManager;
import java.util.List;

public class PlayerDAO extends DAO<Player> {

    public PlayerDAO(EntityManager em){
        super(em);
    }


    @Override
    public boolean add(Player element) {
        try{
            em.persist(element);
            return true;
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }


    public boolean modify(Player element) {
        return false;
    }

    public Player getPlayerById(int id){
        try {
            Player player = em.find(Player.class,id);
            return player;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Player> getAll() {
        try{
            return em.createQuery("SELECT player FROM Player player", Player.class).getResultList();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
