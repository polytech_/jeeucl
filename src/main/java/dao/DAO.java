package dao;

import beans.Team;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDate;
import java.util.List;

public abstract class DAO<T> {

    @PersistenceContext
    protected EntityManager em;

    public DAO(EntityManager entityManager){this.em = entityManager;}

    public abstract boolean add(T element);

    //public abstract boolean modify(Team element);

    public abstract List<T> getAll();

}