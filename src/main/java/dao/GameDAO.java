package dao;

import beans.Game;
import beans.Team;
import exception.ExceptionService;
import hibernateUtil.HibernateService;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.regex.PatternSyntaxException;

public class GameDAO extends DAO<Game> {

    public GameDAO(EntityManager em){
        super(em);
    }

    @Override
    public boolean add(Game element) {
        try{
            em.persist(element);
            return true;
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    public boolean remove(Game element) {
        try{
            em.remove(element);
            return true;
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    public boolean modifyDate(LocalDate date, int id) {
        try{
            Optional<Game> game = getGameById(id);
            game.get().setDate(date);
            em.getTransaction().commit();
            return true;
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    public boolean modifyCityAndStadium(String city, String stadium, int id) {
        try{
            Optional<Game> game = getGameById(id);
            game.get().setCity(city);
            game.get().setStadium(stadium);
            em.getTransaction().commit();
            return true;
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }


    public Optional<Game> getGameById(int id) {
        return Optional.ofNullable(em.find(Game.class,id));
    }

    public Optional<Game> getGameByStadium(String stadium) {
        try {
            return em.createQuery("select g from Game g where g.stadium = :stadium", Game.class)
                    .setParameter("stadium", stadium)
                    .getResultList()
                    .stream()
                    .findFirst();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Game> getAll() {
        try{
            return em.createQuery("SELECT game FROM Game game", Game.class).getResultList();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public void modifyResultDraw(int id, String score) throws ExceptionService {
        if(!score.matches("([0-99]-[0-99])")) {
            throw new ExceptionService("Format de score invalide !");
        }
        Game game = getGameById(id).get();
        String[] splitted = game.getFinalResult().split(" ");
        game.setFinalResult(splitted[0] + " "+score+" " + splitted[2]);

    }

    public void modifyResult(int id, String score, String winner) throws ExceptionService {
        if(!score.matches("([0-99]-[0-99])")) {
            throw new ExceptionService("Format de score invalide !");
        }
        Game game = getGameById(id).get();
        String[] splitted = game.getFinalResult().split(" ");
        if(splitted[0].equals(winner)) {
            game.setFinalResult(winner + score + splitted[2]);
        }
        else {
            game.setFinalResult(splitted[0] + " "+score+" " + winner);
        }


    }
}
