package initializer;

import beans.Game;
import beans.Player;
import beans.Team;
import dao.GameDAO;
import dao.PlayerDAO;
import dao.TeamDAO;
import hibernateUtil.HibernateService;

import javax.persistence.EntityManager;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionListener;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@WebListener()
public class databaseInitializer implements ServletContextListener,
        HttpSessionListener, HttpSessionAttributeListener {

    // Public constructor is required by servlet spec
    public databaseInitializer() {
    }

    // -------------------------------------------------------
    // ServletContextListener implementation
    // -------------------------------------------------------
    public void contextInitialized(ServletContextEvent sce) {

        EntityManager em = HibernateService.emInit();

        TeamDAO teamDAO = new TeamDAO(em);
        PlayerDAO playerDAO = new PlayerDAO(em);
        GameDAO gameDAO = new GameDAO(em);

        if(playerDAO.getAll().isEmpty() || teamDAO.getAll().isEmpty() ||
         gameDAO.getAll().isEmpty()) {

            Player player1 = new Player("Lionnel Messi",
                    50,
                    "Attaquant",
                    15.6);
            Player player2 = new Player("Paul Pogba",
                    23,
                    "Milieu",
                    12.6);
            Player player3 = new Player("Cristiano Ronaldo",
                    48,
                    "Attaquant",
                    14.6);
            Player player4 = new Player("Hugo Loris",
                    0,
                    "Gardient",
                    13.5);
            Player player5 = new Player("Gerrard Piqué",
                    3,
                    "Défenseur",
                    14);
            Player player6 = new Player("Mohammad Salah",
                    38,
                    "Attaquant",
                    14.8);
            Player player7 = new Player("Hazard",
                    30,
                    "Attaquant",
                    15);
            Player player8 = new Player("Mbappé",
                    38,
                    "Attaquant",
                    18);
            Team FC1 = new Team("FC1",12, 3);
            Team FC2 = new Team("FC2", 5, 4);
            Team FC3 = new Team("FC3", 23, 1);
            Team FC4 = new Team("FC4", 20, 2);


            Game game1 = new Game(LocalDate.of(2018, 2, 12),
                    "Munich", "Allianz Stadium", "FC1 2-3 FC2");

            Game game2 = new Game(LocalDate.of(2020, 2, 15),
                    "Casablanca", "Med V", "FC1 2-0 FC3");

            Game game3 = new Game(LocalDate.of(2020, 5, 28),
                    "Paris", "Parc Des Prices", "FC3 4-3 FC2");

            Game game4 = new Game(LocalDate.of(2020, 5, 28),
                    "Madrid", "Bernabeu", "FC4 4-3 FC3");


            player1.setTeam(FC1);
            player2.setTeam(FC1);
            player3.setTeam(FC2);
            player4.setTeam(FC2);
            player5.setTeam(FC3);
            player6.setTeam(FC3);
            player7.setTeam(FC4);
            player8.setTeam(FC4);

            FC1.setGame(game1);
            FC2.setGame(game1);

            FC2.setGame(game2);
            FC4.setGame(game2);

            FC3.setGame(game3);
            FC2.setGame(game3);

            FC1.setGame(game4);
            FC3.setGame(game4);

            em.getTransaction().begin();

            em.persist(player1);
            em.persist(player2);
            em.persist(player3);
            em.persist(player4);
            em.persist(player5);
            em.persist(player6);
            em.persist(player7);
            em.persist(player8);

            em.persist(FC1);
            em.persist(FC2);
            em.persist(FC3);
            em.persist(FC4);

            em.persist(game1);
            em.persist(game2);
            em.persist(game3);
            em.persist(game4);

            em.getTransaction().commit();


        }




    }
}
