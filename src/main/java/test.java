import beans.Game;
import beans.Player;
import beans.Team;
import dao.GameDAO;
import dao.PlayerDAO;
import dao.TeamDAO;
import exception.ExceptionService;
import hibernateUtil.HibernateService;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

public class test {
    public static void main(String[] args) throws ExceptionService {


        EntityManager em = HibernateService.emInit();
        TeamDAO teamDAO = new TeamDAO(em);
        PlayerDAO playerDAO = new PlayerDAO(em);
        GameDAO gameDAO = new GameDAO(em);


//        Player player1 = new Player("name", 2, "position", 12);
//
//        Team team1 = new Team("name", 2,4);
//
//        player1.setTeam(team1);

        em.getTransaction().begin();

        Game game = gameDAO.getGameById(2).get();
        gameDAO.modifyResultDraw(2, "3--3");

        em.getTransaction().commit();
    }
}
