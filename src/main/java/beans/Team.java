package beans;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Team {
    @Id
    private String teamName;
    private int nbOfVictories;
    private int ranking;

    @OneToMany(mappedBy = "team")
    private List<Player> players;

    @ManyToOne
    private Game game;

    public Team() {
    }

    public Team(String teamName, int nbOfVictories, int ranking) {
        this.teamName = teamName;
        this.nbOfVictories = nbOfVictories;
        this.ranking = ranking;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String nomEquipe) {
        this.teamName = nomEquipe;
    }

    public int getNbOfVictories() {
        return nbOfVictories;
    }

    public void setNbOfVictories(int nombreVictoire) {
        this.nbOfVictories = nombreVictoire;
    }

    public int getRanking() {
        return ranking;
    }

    public void setRanking(int classement) {
        this.ranking = classement;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> playerList) {
        this.players = playerList;
    }

    public void addPlayer(Player player) {
        if(players==null) {
            players = new ArrayList<>();
        }
        players.add(player);
    }
    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }
}
