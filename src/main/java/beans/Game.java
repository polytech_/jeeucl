package beans;

import org.hibernate.annotations.OnDelete;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Entity

public class Game {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int idGame;
    private LocalDate date;
    private String city;
    private String stadium;
    private String finalResult;

    @OneToMany(mappedBy = "game")
    private List<Team> teamList;

    public Game() {
    }

    public Game(LocalDate date, String city, String stadium, String finalResult) {
        this.date = date;
        this.city = city;
        this.stadium = stadium;
        this.finalResult = finalResult;
    }

    public int getIdGame() {
        return idGame;
    }

    public void setIdGame(int idMatch) {
        this.idGame = idMatch;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStadium() {
        return stadium;
    }

    public void setStadium(String stade) {
        this.stadium = stade;
    }

    public String getFinalResult() {
        return finalResult;
    }

    public void setFinalResult(String résultats) {
        this.finalResult = résultats;
    }

    public List<Team> getTeamList() {
        return teamList;
    }

    public void setTeamList(List<Team> teamList) {
        this.teamList = teamList;
    }
}
