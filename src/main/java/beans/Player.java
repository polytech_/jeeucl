package beans;

import javax.persistence.*;
import java.util.List;

@Entity
public class Player {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int idPlayer;

    private String playerName;
    private int goals;
    private String position;
    private double averageScore;

    @ManyToOne(cascade=CascadeType.ALL)
    private Team team;

    public Player() {
    }

    public Player(String playerName, int goals, String position, double averageScore) {
        this.playerName = playerName;
        this.goals = goals;
        this.position = position;
        this.averageScore = averageScore;

    }

    public int getIdPlayer() {
        return idPlayer;
    }

    public void setIdPlayer(int idJoueur) {
        this.idPlayer = idJoueur;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public int getGoals() {
        return goals;
    }

    public void setGoals(int buts) {
        this.goals = buts;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String role) {
        this.position = role;
    }

    public double getAverageScore() {
        return averageScore;
    }

    public void setAverageScore(double noteMoyenne) {
        this.averageScore = noteMoyenne;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }


}
