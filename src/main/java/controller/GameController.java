package controller;

import beans.Game;
import dao.GameDAO;
import hibernateUtil.HibernateService;

import javax.servlet.ServletException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;

public class GameController extends javax.servlet.http.HttpServlet {

    private GameDAO gameDAO;

    @Override
    public void init() throws ServletException {
        gameDAO = new GameDAO(HibernateService.emInit());
    }

    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
//        int id = Integer.parseInt(request.getParameter("id"));
//        PrintWriter out = response.getWriter();
//        out.println("L'id du match à modifier est :"+ id+"");

    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
//        if(request.getParameter("action").equals("display")) {
        Collection<Game> allGames = gameDAO.getAll();
        request.setAttribute("allGames", new ArrayList<>(allGames));
        getServletContext().getRequestDispatcher("/games.jsp").forward(request, response);
//        } else if(request.getParameter("action").equals("modify")){
//            doPost(request, response);
//        }

    }
}
