package controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "LoginController")
public class LoginController extends HttpServlet {


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String adminUsername = getInitParameter("login");
        String adminPassword = getInitParameter("password");

        System.out.println(username+"-"+password+";"+adminUsername+"-"+adminPassword);

        if(username.equals(adminUsername) && password.equals(adminPassword)) {
            //request.getSession().setAttribute("userConnected", );
            response.sendRedirect("/gamesLogged");
        }
        else {
            response.sendRedirect("logIn.jsp");
        }



    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("logIn.jsp").forward(request, response);

    }
}
