package controller;

import beans.Player;
import beans.Team;
import dao.PlayerDAO;
import dao.TeamDAO;
import hibernateUtil.HibernateService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

@WebServlet(name = "Servlet")
public class PlayerController extends HttpServlet {
    private PlayerDAO playerDAO;

    @Override
    public void init() throws ServletException {
        playerDAO = new PlayerDAO(HibernateService.emInit());
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Collection<Player> allPlayers = playerDAO.getAll();
        request.setAttribute("allPlayers", new ArrayList<>(allPlayers));
        getServletContext().getRequestDispatcher("/players.jsp").forward(request, response);
    }
}
