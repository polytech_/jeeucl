package controller;

import beans.Game;
import beans.Team;
import dao.GameDAO;
import dao.TeamDAO;
import hibernateUtil.HibernateService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

@WebServlet(name = "TeamController")
public class TeamController extends HttpServlet {

    private TeamDAO teamDAO;

    @Override
    public void init() throws ServletException {
        teamDAO = new TeamDAO(HibernateService.emInit());
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Collection<Team> allTeams = teamDAO.getAll();
        request.setAttribute("allTeams", new ArrayList<>(allTeams));
        getServletContext().getRequestDispatcher("/teams.jsp").forward(request, response);

    }
}
