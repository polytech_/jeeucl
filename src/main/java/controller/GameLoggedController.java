package controller;

import beans.Game;
import dao.GameDAO;
import hibernateUtil.HibernateService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;

@WebServlet(name = "GameLoggedController")
public class GameLoggedController extends HttpServlet {

    private GameDAO gameDAO;

    @Override
    public void init() throws ServletException {
        gameDAO = new GameDAO(HibernateService.emInit());
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        PrintWriter out = response.getWriter();
        out.println("L'id du match à modifier est :"+ id+"");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Collection<Game> allGames = gameDAO.getAll();
        request.setAttribute("allGames", new ArrayList<>(allGames));
        getServletContext().getRequestDispatcher("/gamesLogged.jsp").forward(request, response);
    }
}
