package hibernateUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class HibernateService {

    public static EntityManager emInit() {
        EntityManagerFactory entityManagerFactory =
                Persistence.createEntityManagerFactory("test");

        return entityManagerFactory.createEntityManager();

    }

}
