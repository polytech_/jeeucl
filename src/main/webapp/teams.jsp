<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Equipes</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>


<div class="container-fluid" style="width: 70%">
    <br>
    <h3>Liste des Equipes :</h3>
    <br>
    <table class="table table-hover table-striped">
        <thead class="thead-dark">
        <tr>
            <th style="width: 20%; text-align: center" scope="col">Nom</th>
            <th style="width: 20%; text-align: center" scope="col">Victoires</th>
            <th style="width: 20%; text-align: center" scope="col">Classement</th>
            <th style="width: 20%; text-align: center" scope="col">Nb Joueurs</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${allTeams}" var="team">
            <tr>
                <td style="text-align: center">${team.teamName}</td>
                <td style="text-align: center">${team.nbOfVictories}</td>
                <td style="text-align: center">${team.ranking}</td>
                <td style="text-align: center">${team.players.size()}</td>
            </tr>

        </c:forEach>
        </tbody>
    </table>

</div>

<div class="row justify-content-center">
    <div class="col-2">
        <a style="margin-left: 25%" href="${pageContext.request.contextPath}/logIn.jsp" class="btn btn-primary stretched-link">Se connecter</a>
    </div>
</div>


<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>
